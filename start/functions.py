def diferencia (a,b,c):
    dif1 = abs(a-b)
    dif2 = abs(b-c)
    dif3 = abs(a-c)
    return min(dif1, dif2, dif3)

res_dif1 = diferencia(1,10,100)
res_dif2 = diferencia(1,10,10)
res_dif3 = diferencia(5, 6, 7)

print(res_dif1, res_dif2, res_dif3)